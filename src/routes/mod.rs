pub mod linear_program;
pub mod responses;

#[derive(Serialize, Deserialize)]
pub struct Page<T> {
    pub total_pages: usize,
    pub elements: Vec<T>,
}
