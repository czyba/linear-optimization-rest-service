use rocket::request::Request;
use rocket::response::{Response, Responder};
use rocket::http::Status;
use serde::Serialize;

use super::rest_api_error::RestApiError;
use super::get_json_response;

pub enum GetResponse<T> {
    Ok(T),
    NoContent,
    InternalServerError(RestApiError),
}

impl<'r, T: Serialize> Responder<'r> for GetResponse<T> {
    fn respond_to(self, request: &Request) -> Result<Response<'r>, rocket::http::Status> {
        return match self {
            GetResponse::NoContent => Response
                ::build()
                .status(Status::NoContent)
                .ok(),
            GetResponse::Ok(result) => get_json_response(result, &request)?
                .status(Status::Ok)
                .ok(),
            GetResponse::InternalServerError(err) => get_json_response(Err(err) as Result<T, RestApiError>, &request)?
                .status(Status::InternalServerError)
                .ok(),
        }
    }
}

impl<T, E: Into<RestApiError>> std::convert::From<std::result::Result<T, E>> for GetResponse<T> {
    fn from(result: Result<T, E>) -> Self {
        match result {
            Ok(v) => GetResponse::Ok(v),
            Err(e) => GetResponse::InternalServerError(e.into())
        }
    }
}

impl<T, E: Into<RestApiError>> std::convert::From<Result<Option<T>, E>> for GetResponse<T> {
    fn from(result: Result<Option<T>, E>) -> Self {
        match result {
            Ok(Some(v)) => GetResponse::Ok(v),
            Ok(None) => GetResponse::NoContent,
            Err(e) => GetResponse::InternalServerError(e.into()),
        }
    }
}
