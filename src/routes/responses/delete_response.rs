use rocket::request::Request;
use rocket::response::{Response, Responder};
use rocket::http::Status;
use serde::Serialize;

use super::rest_api_error::RestApiError;
use super::get_json_response;

pub enum DeleteResponse<T> {
    Ok(T),
    NoContent,
    InternalServerError(RestApiError),
}

impl<'r, T: Serialize> Responder<'r> for DeleteResponse<T> {
    fn respond_to(self, request: &Request) -> Result<Response<'r>, rocket::http::Status> {
        return match self {
            DeleteResponse::Ok(result) => get_json_response(result, &request)?
                .status(Status::Ok)
                .ok(),
            DeleteResponse::NoContent => Response::build()
                .status(Status::NoContent)
                .ok(),
            DeleteResponse::InternalServerError(err) => get_json_response(Err(err) as Result<T, RestApiError>, &request)?
                .status(Status::InternalServerError)
                .ok(),
        }
    }
}

impl<T, E: Into<RestApiError>> std::convert::From<Result<Option<T>, E>> for DeleteResponse<T> {
    fn from(val: Result<Option<T>, E>) -> DeleteResponse<T> {
        match val {
            Ok(Some(v)) => DeleteResponse::Ok(v),
            Ok(None) => DeleteResponse::NoContent,
            Err(e) => DeleteResponse::InternalServerError(e.into()),
        }
    }
}
