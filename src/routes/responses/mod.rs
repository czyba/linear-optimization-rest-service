pub mod delete_response;
pub mod get_response;
pub mod post_response;
pub mod put_response;
pub mod rest_api_error;

use rocket_contrib::json::Json;
use rocket::request::Request;
use rocket::response::{Response, ResponseBuilder, Responder};
use serde::Serialize;

fn get_json_response<'r, T: Serialize>(
    value: T,
    request: &Request
) -> Result<ResponseBuilder<'r>, rocket::http::Status> {
    Ok(Response::build_from(Json(value).respond_to(&request)?))
}
