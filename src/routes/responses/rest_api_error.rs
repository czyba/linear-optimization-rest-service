use super::super::super::services::ServiceError;

#[derive(Serialize, Deserialize, Clone)]
pub enum RestApiError {
    CouldNotAquireResource(&'static str),
    NoCorrespondingResource,
    CalculationError,
}

impl std::convert::From<ServiceError> for RestApiError {
    fn from(val: ServiceError) -> Self {
        match val {
            ServiceError::CouldNotAcquireConnection => RestApiError::CouldNotAquireResource("Could connect to database"),
            ServiceError::SerializationError => RestApiError::CouldNotAquireResource("Could not deserialize/serialize resource from database"),
            ServiceError::GLPKError => RestApiError::CalculationError,
        }
    }
}
