use rocket::request::Request;
use rocket::response::{Response, Responder};
use rocket::http::Status;
use serde::Serialize;

use super::rest_api_error::RestApiError;
use super::get_json_response;

pub enum PostResponse<T> {
    Created(T, String),
    // BadRequest(RestApiError),
    InternalServerError(RestApiError),
}

impl<'r, T: Serialize> Responder<'r> for PostResponse<T> {
    fn respond_to(self, request: &Request) -> Result<Response<'r>, rocket::http::Status> {
        return match self {
            PostResponse::Created(result, location) => get_json_response(result, &request)?
                .status(Status::Created)
                .header(rocket::http::hyper::header::Location(location))
                .ok(),
            // PostResponse::BadRequest(err) => get_json_response(Err(err) as Result<T, RestApiError>, &request)?
            //     .status(Status::BadRequest)
            //     .ok(),
            PostResponse::InternalServerError(err) => get_json_response(Err(err) as Result<T, RestApiError>, &request)?
                .status(Status::InternalServerError)
                .ok(),
        }
    }
}

impl<T, E: Into<RestApiError>> std::convert::From<E> for PostResponse<T> {
    fn from(val: E) -> PostResponse<T> {
        PostResponse::InternalServerError(val.into())
    }
}
