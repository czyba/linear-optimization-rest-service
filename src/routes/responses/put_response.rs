use rocket::request::Request;
use rocket::response::{Response, Responder};
use rocket::http::Status;
use serde::Serialize;

use super::rest_api_error::RestApiError;
use super::get_json_response;

pub enum PutResponse<T> {
    Ok(T),
    NotFound(RestApiError),
    InternalServerError(RestApiError),
}

impl<'r, T: Serialize> Responder<'r> for PutResponse<T> {
    fn respond_to(self, request: &Request) -> Result<Response<'r>, rocket::http::Status> {
        return match self {
            PutResponse::Ok(result) => get_json_response(result, &request)?
                .status(Status::Ok)
                .ok(),
            PutResponse::NotFound(err) => get_json_response(Err(err) as Result<T, RestApiError>, &request)?
                .status(Status::NotFound)
                .ok(),
            PutResponse::InternalServerError(err) => get_json_response(Err(err) as Result<T, RestApiError>, &request)?
                .status(Status::InternalServerError)
                .ok(),
        }
    }
}

impl<T, E: Into<RestApiError>> std::convert::From<Result<T, E>> for PutResponse<T> {
    fn from(val: Result<T, E>) -> PutResponse<T> {
        match val {
            Ok(v) => PutResponse::Ok(v),
            Err(e) => match e.into() {
                RestApiError::NoCorrespondingResource => PutResponse::NotFound(RestApiError::NoCorrespondingResource),
                any_other => PutResponse::InternalServerError(any_other)
            },
        }
    }
}
