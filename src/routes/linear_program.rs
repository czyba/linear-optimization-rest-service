use std::collections::BTreeMap;
use super::Page;
use super::super::services::linear_program;
use super::responses::delete_response::DeleteResponse;
use super::responses::get_response::GetResponse;
use super::responses::post_response::PostResponse;
use super::responses::put_response::PutResponse;
use rocket_contrib::json::Json;
use maud::Markup;
use std::fmt;

#[derive(Serialize, Deserialize, Clone)]
pub enum ObjectiveFunctionGoal {
    MAX,
    MIN
}

impl fmt::Display for ObjectiveFunctionGoal {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
       match *self {
           ObjectiveFunctionGoal::MAX => write!(f, "max"),
           ObjectiveFunctionGoal::MIN => write!(f, "min"),
       }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub enum RowComparison {
    LE(f64),
    GE(f64)
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Constraint {
    pub values: BTreeMap<usize, f64>,
    pub comparator: RowComparison,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct ObjectiveFunction {
    pub goal: ObjectiveFunctionGoal,
    pub coefficients: BTreeMap<usize, f64>,
}

#[derive(Serialize, Deserialize, Clone)]
pub enum VariableBounds {
    NoBounds,
    LowerBound(f64),
    UpperBound(f64),
    BothBounds(f64, f64),
}

#[derive(Serialize, Deserialize, Clone)]
pub enum VariableType {
    BOOL,
    INTEGER,
    RATIONAL
}

#[derive(Serialize, Deserialize, Clone)]
pub struct VariableDescription {
    pub bounds: VariableBounds,
    pub variable_type: VariableType,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct LinearProgram {
    pub id: i32,
    pub name: Option<String>,
    pub objective_function: ObjectiveFunction,
    pub constraints: Vec<Constraint>,
    pub variable_bounds: BTreeMap<usize, VariableDescription>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct InputLinearProgram {
    pub id: Option<i32>,
    pub name: Option<String>,
    pub objective_function: ObjectiveFunction,
    pub constraints: Vec<Constraint>,
    pub variable_bounds: BTreeMap<usize, VariableDescription>,
}

impl LinearProgram {
    pub fn from_id_and_data(id: i32, data: InputLinearProgram) -> LinearProgram {
        LinearProgram {
            id,
            name: data.name,
            objective_function: data.objective_function,
            constraints: data.constraints,
            variable_bounds: data.variable_bounds,
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub enum ResultStatus {
    Optimal(f64),
    Feasible(f64),
    Infeasible,
    NoFeasible,
    Unbounded(f64),
    Undef
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Solution {
    pub status: ResultStatus
}

#[get("/linear-programs?<page>&<page_size>")]
pub fn get_linear_programs(
    page: Option<u32>,
    page_size: Option<u32>
) -> GetResponse<Page<LinearProgram>> {
    let data = linear_program::get_linear_programs(
        if let Some(page_value) = page { page_value } else { 0 },
        if let Some(page_size) = page_size { page_size } else { 10 },
    );
    GetResponse::from(data)
}

#[get("/linear-programs/<id>")]
pub fn get_linear_program(
    id: i32,
) -> GetResponse<LinearProgram> {
    let data = linear_program::get_linear_program(id);
    GetResponse::from(data)
}

#[get("/linear-programs/<id>/page")]
pub fn hello(id: i32) -> Markup {
    let data = linear_program::get_linear_program(id);
    let data = match data {
        Ok(v) => v,
        Err(_) => return html! {
            h1 { "Error while loading linear program" }
        }
    };
    let data = match data {
        Some(v) => v,
        None => return html! {
            h1 { "No linear program at this address" }
        }
    };
    return html! {
        h1 { "Linear Program: " (data.name.unwrap_or("".to_owned())) "!" }
        p {
            "Objective function: " (data.objective_function.goal) " " @for entry in data.objective_function.coefficients { (entry.1) " * x" (entry.0) " + " }
        }
    }
}

#[get("/linear-programs/<id>/solution")]
pub fn get_linear_program_solution(
    id: i32,
) -> GetResponse<Solution> {
    let data = linear_program::get_linear_program_solution(id);
    GetResponse::from(data)
}

#[post("/linear-programs", data = "<data>")]
pub fn post_linear_program(
    data: Json<InputLinearProgram>
) -> PostResponse<LinearProgram> {
    match linear_program::add_new_linear_program(data.0) {
        Ok(v) => {
            let redirect = format!("/linear-programs/{}", v.id);
            PostResponse::Created(v, redirect)
        },
        Err(e) => e.into()
    }
}

#[put("/linear-programs/<id>", data = "<data>")]
pub fn put_linear_program(
    id: i32,
    data: Json<InputLinearProgram>
) -> PutResponse<LinearProgram> {
    PutResponse::from(linear_program::replace_linear_program(id, data.0))
}

#[delete("/linear-programs/<id>")]
pub fn delete_linear_program(
    id: i32
) -> DeleteResponse<LinearProgram> {
    DeleteResponse::from(linear_program::remove_linear_program(id))
}
