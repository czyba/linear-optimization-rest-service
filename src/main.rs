#![feature(custom_attribute, proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
extern crate rocket_contrib;
#[macro_use] extern crate serde_derive;
extern crate uuid;
extern crate libc;
#[macro_use]
extern crate diesel;
extern crate r2d2;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate maud;

mod routes;
mod services;
mod glpk;

use rocket::config::{Config, Environment};
use routes::linear_program;
use services::db::get_connection_pool;

fn main() {
    let config = Config::build(Environment::Staging)
        .address("127.0.0.1")
        .port(8080)
        .workers(10)
        .unwrap();
    // Initialize the connection pool before starting rocket in order to avoid
    // runtime issues
    get_connection_pool();
    println!("here");
    rocket::custom(config)
        .mount("/api", routes![
            linear_program::hello,
            linear_program::get_linear_programs,
            linear_program::get_linear_program,
            linear_program::post_linear_program,
            linear_program::put_linear_program,
            linear_program::delete_linear_program,
            linear_program::get_linear_program_solution,
        ])
        .launch();
}
