use super::glpk_functions::*;
use super::GLPKError;
use super::{MAX_NUM_CONSTRAINTS, MAX_NUM_VARIABLES};
use super::GLPKConstraintBounds;
use super::GLPKVariableBounds;
use super::GLPKVariableType;
use super::GLPKObjectiveDirection;
use super::GLPKStatus;

pub struct InternalGLPKProblem {
    problem_ptr: *mut glp_prob,
}

impl InternalGLPKProblem {

    pub fn new_empty() -> Result<InternalGLPKProblem, GLPKError> {
        unsafe {
            let ptr = glp_create_prob();
            if ptr.is_null() {
                return Err(GLPKError::GLPKError);
            }
            let problem = InternalGLPKProblem {
                problem_ptr: ptr,
            };
            return Ok(problem);
        }
    }

    pub fn set_problem_row_unchecked(
        &mut self,
        index: usize,
        num_coefficients: usize,
        indices: &Vec<libc::c_int>,
        coefficients: &Vec<f64>,
    ) {
        unsafe {
            glp_set_mat_row(
                self.problem_ptr,
                (index + 1) as libc::c_int,
                num_coefficients as libc::c_int,
                indices.as_ptr(),
                coefficients.as_ptr(),
            );
        }
    }

    pub fn set_constraint_bounds_unchecked(
        &mut self,
        index: usize,
        constraint_bounds: &GLPKConstraintBounds,
    ) {
        unsafe {
            glp_set_row_bnds(
                self.problem_ptr,
                (index + 1) as libc::c_int,
                constraint_bounds.bound_type(),
                constraint_bounds.get_lower_bound_or_null(),
                constraint_bounds.get_upper_bound_or_null(),
            )
        }
    }

    fn translate_and_check_variable_index(&self, index: usize) -> Result<usize, GLPKError> {
        let num_vars = self.get_num_variables();
        if num_vars <= index {
            return Err(GLPKError::IndexOutOfBounds(num_vars, index));
        }
        Ok(index + 1)
    }

    /// Returns the number of variables of the problem.
    ///
    /// # Examples
    ///
    /// ```
    /// use glpk_program::internal::InternalGLPKProblem;
    ///
    /// let mut problem = InternalGLPKProblem::new_empty().unwrap();
    /// assert_eq!(0, problem.get_num_constraints());
    /// assert_eq!(Ok(0), problem.add_num_constraints(3));
    /// assert_eq!(3, problem.get_num_constraints());
    /// assert_eq!(Ok(3), problem.add_num_constraints(2));
    /// assert_eq!(5, problem.get_num_constraints());
    /// ```
    pub fn get_num_constraints(&self) -> usize {
        unsafe {
            return glp_get_num_rows(self.problem_ptr) as usize;
        }
    }

    /// Adds the given number of constraints to the problem.
    /// If the given number would result in the problem having more than
    /// `MAX_NUM_CONSTRAINTS` constraints, an error is returned instead.
    ///
    /// Returns the index of the first added constraint starting at 0.
    ///
    /// # Examples
    ///
    /// ```
    /// use glpk_program::internal::InternalGLPKProblem;
    /// use glpk_program::MAX_NUM_CONSTRAINTS;
    /// use glpk_program::Error::GLPKError;
    ///
    /// let mut problem = InternalGLPKProblem::new_empty().unwrap();
    /// assert_eq!(Err(GLPKError::InvalidArgument), problem.add_num_constraints(0));
    /// assert_eq!(Ok(0), problem.add_num_constraints(3));
    /// assert_eq!(Ok(3), problem.add_num_constraints(2));
    /// assert_eq!(Err(GLPKError::TooManyConstraints), problem.add_num_constraints(MAX_NUM_CONSTRAINTS));
    /// ```
    pub fn add_num_constraints(&mut self, num: usize) -> Result<usize, GLPKError> {
        unsafe {
            let current_num_rows = self.get_num_constraints();
            if num == 0 {
                return Err(GLPKError::InvalidArgument);
            }
            if num > MAX_NUM_CONSTRAINTS || num + current_num_rows > MAX_NUM_CONSTRAINTS {
                return Err(GLPKError::TooManyConstraints);
            }
            let input_arg = num as libc::c_int;
            let ret = glp_add_rows(self.problem_ptr, input_arg);
            return Ok((ret - 1) as usize);
        }
    }

    /// Returns the number of variables of the problem.
    ///
    /// # Examples
    ///
    /// ```
    /// use glpk_program::internal::InternalGLPKProblem;
    ///
    /// let mut problem = InternalGLPKProblem::new_empty().unwrap();
    /// assert_eq!(0, problem.get_num_variables());
    /// assert_eq!(Ok(0), problem.add_num_variables(3));
    /// assert_eq!(3, problem.get_num_variables());
    /// assert_eq!(Ok(3), problem.add_num_variables(2));
    /// assert_eq!(5, problem.get_num_variables());
    /// ```
    pub fn get_num_variables(&self) -> usize {
        unsafe {
            return glp_get_num_cols(self.problem_ptr) as usize;
        }
    }

    /// Adds the given number of variables to the problem.
    /// If the given number would result in the problem having more than
    /// `MAX_NUM_VARIABLES` variables, an error is returned instead.
    ///
    /// Returns the index of the first added variable starting at 0.
    ///
    /// # Examples
    ///
    /// ```
    /// use glpk_program::internal::InternalGLPKProblem;
    /// use glpk_program::MAX_NUM_VARIABLES;
    /// use glpk_program::Error::GLPKError;
    ///
    /// let mut problem = InternalGLPKProblem::new_empty().unwrap();
    /// assert_eq!(Err(GLPKError::InvalidArgument), problem.add_num_variables(0));
    /// assert_eq!(Ok(0), problem.add_num_variables(3));
    /// assert_eq!(Ok(3), problem.add_num_variables(2));
    /// assert_eq!(Err(GLPKError::TooManyConstraints), problem.add_num_variables(MAX_NUM_VARIABLES));
    /// ```
    pub fn add_num_variables(&mut self, num: usize) -> Result<usize, GLPKError> {
        let current_num_cols = self.get_num_variables();
        if num == 0 {
            return Err(GLPKError::InvalidArgument);
        }
        if num > MAX_NUM_VARIABLES || num + current_num_cols > MAX_NUM_VARIABLES {
            return Err(GLPKError::TooManyVariables);
        }
        let input_arg = num as libc::c_int;
        unsafe {
            let ret = glp_add_cols(self.problem_ptr, input_arg);
            return Ok((ret - 1) as usize);
        }
    }

    pub fn set_variable_bounds(&mut self, index: usize, bounds: GLPKVariableBounds) -> Result<(), GLPKError> {
        let translated_index = self.translate_and_check_variable_index(index)?;
        unsafe {
            glp_set_col_bnds(
                self.problem_ptr,
                translated_index as libc::c_int,
                bounds.bound_type(),
                bounds.get_lower_bound_or_null(),
                bounds.get_upper_bound_or_null()
            )
        }
        Ok(())
    }

    pub fn set_variable_coefficient(&mut self, index: usize, coefficient: f64) -> Result<(), GLPKError> {
        let translated_index = self.translate_and_check_variable_index(index)?;
        unsafe {
            glp_set_obj_coef(
                self.problem_ptr,
                translated_index as libc::c_int,
                coefficient,
            )
        }
        Ok(())
    }

    pub fn set_variable_type(&mut self, index: usize, variable_type: GLPKVariableType) -> Result<(), GLPKError> {
        let translated_index = self.translate_and_check_variable_index(index)?;
        unsafe {
            glp_set_col_kind(
                self.problem_ptr,
                translated_index as libc::c_int,
                variable_type.to_glpk_kind(),
            )
        }
        Ok(())
    }

    pub fn solve(&mut self)  {
        unsafe {
            glp_simplex(self.problem_ptr, std::ptr::null());
            glp_intopt(self.problem_ptr, std::ptr::null());
        }
    }

    pub fn set_objective_direction(&mut self, dir: GLPKObjectiveDirection)  {
        unsafe {
            glp_set_obj_dir(self.problem_ptr, dir.to_glpk_dir());
        }
    }

    pub fn get_status(&self) -> GLPKStatus {
        unsafe {
            let int_status = glp_get_status(self.problem_ptr);
            GLPKStatus::from_int_status_and_objective_value(int_status, glp_get_obj_val(self.problem_ptr))
        }
    }
}

impl std::ops::Drop for InternalGLPKProblem {
    fn drop(&mut self) {
        unsafe {
            glp_delete_prob(self.problem_ptr);
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use super::super::*;

    #[test]
    fn test_add_num_constraints_returning_an_invalid_argument_error_on_0() {
        let mut problem = InternalGLPKProblem::new_empty().unwrap();
        assert_eq!(Err(GLPKError::InvalidArgument), problem.add_num_constraints(0usize));
    }

    #[test]
    fn test_add_num_constraints_returning_the_starting_index_on_success() {
        let mut problem = InternalGLPKProblem::new_empty().unwrap();
        assert_eq!(Ok(0), problem.add_num_constraints(3usize));
        assert_eq!(Ok(3), problem.add_num_constraints(2usize));
    }

    #[test]
    fn test_add_num_constraints_returning_too_many_constraint_on_limit_break() {
        let mut problem = InternalGLPKProblem::new_empty().unwrap();
        assert_eq!(Ok(0), problem.add_num_constraints(1usize));
        assert_eq!(Err(GLPKError::TooManyConstraints), problem.add_num_constraints(MAX_NUM_CONSTRAINTS));
    }

    #[test]
    fn test_add_num_variables_returning_an_invalid_argument_error_on_0() {
        let mut problem = InternalGLPKProblem::new_empty().unwrap();
        assert_eq!(Err(GLPKError::InvalidArgument), problem.add_num_variables(0usize));
    }

    #[test]
    fn test_add_num_variables_returning_the_starting_index_on_success() {
        let mut problem = InternalGLPKProblem::new_empty().unwrap();
        assert_eq!(Ok(0), problem.add_num_variables(3usize));
        assert_eq!(Ok(3), problem.add_num_variables(2usize));
    }

    #[test]
    fn test_add_num_variables_returning_too_many_constraint_on_limit_break() {
        let mut problem = InternalGLPKProblem::new_empty().unwrap();
        assert_eq!(Ok(0), problem.add_num_variables(1usize));
        assert_eq!(Err(GLPKError::TooManyVariables), problem.add_num_variables(MAX_NUM_VARIABLES));
    }

    #[test]
    fn test_get_num_variables_returning_number_of_added_variables() {
        let mut problem = InternalGLPKProblem::new_empty().unwrap();
        assert_eq!(0, problem.get_num_variables());
        assert_eq!(Ok(0), problem.add_num_variables(3));
        assert_eq!(3, problem.get_num_variables());
        assert_eq!(Ok(3), problem.add_num_variables(2));
        assert_eq!(5, problem.get_num_variables());
    }

    #[test]
    fn test_get_num_csontraints_returning_number_of_added_constraints() {
        let mut problem = InternalGLPKProblem::new_empty().unwrap();
        assert_eq!(0, problem.get_num_constraints());
        assert_eq!(Ok(0), problem.add_num_constraints(3));
        assert_eq!(3, problem.get_num_constraints());
        assert_eq!(Ok(3), problem.add_num_constraints(2));
        assert_eq!(5, problem.get_num_constraints());
    }



}
