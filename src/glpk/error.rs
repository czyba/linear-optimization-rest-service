
#[derive(Debug, PartialEq)]
pub enum GLPKError {
    TooManyConstraints,
    TooManyVariables,
    InvalidArgument,
    GLPKError,
    IndexOutOfBounds(usize, usize),
}
