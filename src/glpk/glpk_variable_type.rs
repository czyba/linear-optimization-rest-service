#[derive(Clone, Debug)]
pub enum GLPKVariableType {
    Continous,
    Integer,
    Boolean
}

impl GLPKVariableType {
    pub fn to_glpk_kind(&self) -> libc::c_int {
        match self {
            GLPKVariableType::Continous => 1,
            GLPKVariableType::Integer => 2,
            GLPKVariableType::Boolean => 3,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn continous_returns_1_as_glpk_kind() {
        assert_eq!(1, GLPKVariableType::Continous.to_glpk_kind())
    }

    #[test]
    fn intger_returns_2_as_glpk_kind() {
        assert_eq!(2, GLPKVariableType::Integer.to_glpk_kind())
    }

    #[test]
    fn boolean_returns_3_as_glpk_kind() {
        assert_eq!(3, GLPKVariableType::Boolean.to_glpk_kind())
    }
}
