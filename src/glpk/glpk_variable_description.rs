use super::glpk_variable_bounds::GLPKVariableBounds;
use super::glpk_variable_type::GLPKVariableType;

#[derive(Debug)]
pub struct GLPKVariableDescription {
    pub variable_bounds: GLPKVariableBounds,
    pub variable_type: GLPKVariableType,
    pub variable_coefficient: f64,
}