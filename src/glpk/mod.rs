use libc;

pub mod error;
pub mod glpk_constraint_bounds;
pub mod glpk_constraint;
pub mod glpk_status;
mod glpk_functions;
pub mod glpk_objective_direction;
pub mod glpk_variable_bounds;
pub mod glpk_variable_description;
pub mod glpk_variable_type;
mod internal;

pub use self::error::*;
pub use self::glpk_constraint_bounds::*;
pub use self::glpk_constraint::*;
pub use self::glpk_objective_direction::*;
pub use self::glpk_variable_bounds::*;
pub use self::glpk_variable_description::*;
pub use self::glpk_variable_type::*;
pub use self::glpk_status::*;
use self::internal::InternalGLPKProblem;

/// Maximum number of constraints that are allowed to be added
const MAX_NUM_CONSTRAINTS: usize = 0x8000usize;
/// Maximum number of variables that are allowed to be added
const MAX_NUM_VARIABLES: usize = 0x8000usize;

pub struct GLPKProblem {
    internal: InternalGLPKProblem
}

impl GLPKProblem {
    pub fn new(
            dir: GLPKObjectiveDirection
    ) -> Result<GLPKProblem, GLPKError> {
        let mut problem = GLPKProblem::new_empty()?;
        problem.internal.set_objective_direction(dir);
        return Ok(problem);
    }

    fn new_empty() -> Result<GLPKProblem, GLPKError> {
        Ok(GLPKProblem {
            internal: InternalGLPKProblem::new_empty()?
        })
    }

    pub fn add_constraint(&mut self, constraints: &Vec<GLPKConstraint>) -> Result<usize, GLPKError> {
        // First check sanity of constraints
        let num_variables = self.get_num_variables();
        if constraints.is_empty() {
            return Ok(num_variables)
        }

        for constraint in constraints {
            constraint.is_sane(num_variables)?;
        }

        let start_index = self.internal.add_num_constraints(constraints.len())?;
        let mut run_index = start_index;

        for constraint in constraints {
            let num_coefficients = constraint.coefficients.len();
            let mut coefficients: Vec<libc::c_double> = Vec::with_capacity(num_coefficients + 1);
            coefficients.push(0f64);
            let mut indices: Vec<libc::c_int> = Vec::with_capacity(num_coefficients + 1);
            indices.push(0);
            for (index, value) in constraint.coefficients.iter() {
                indices.push((*index + 1) as libc::c_int);
                coefficients.push(*value);
            }
            println!("{:?}", indices);
            println!("{:?}", coefficients);

            self.internal.set_problem_row_unchecked(
                run_index,
                num_coefficients,
                &indices,
                &coefficients,
            );
            self.internal.set_constraint_bounds_unchecked(
                run_index,
                &constraint.constraint_bounds,
            );
            run_index += 1;
        }

        return Ok(start_index);
    }

    pub fn get_num_constraints(&self) -> usize {
        self.internal.get_num_constraints()
    }

    pub fn get_num_variables(&self) -> usize {
        self.internal.get_num_variables()
    }

    pub fn add_variables(&mut self, new_variables: Vec<GLPKVariableDescription>) -> Result<usize, GLPKError> {
        let start_index = self.internal.add_num_variables(new_variables.len())?;
        let mut run_index = start_index;
        for var in new_variables {
            // Unwrap is safe here because we ourselves added the respective indices
            self.internal.set_variable_type(run_index, var.variable_type).unwrap();
            self.internal.set_variable_coefficient(run_index, var.variable_coefficient).unwrap();
            self.internal.set_variable_bounds(run_index, var.variable_bounds).unwrap();
            run_index += 1;
        }
        return Ok(start_index);
    }

    pub fn solve(&mut self) {
        self.internal.solve()
    }

    pub fn get_status(&self) -> GLPKStatus {
        self.internal.get_status()
    }
}
