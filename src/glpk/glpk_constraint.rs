use std::collections::BTreeMap;
use super::error::GLPKError;
use super::glpk_constraint_bounds::GLPKConstraintBounds;

#[derive(Debug)]
pub struct GLPKConstraint {
    pub constraint_bounds: GLPKConstraintBounds,
    pub coefficients: BTreeMap<usize, f64>,
}

impl GLPKConstraint {
    pub fn is_sane(&self, num_variables: usize) -> Result<(), GLPKError> {
        let max_index = self.coefficients.iter().map(|(k,_v)| k).max();
        if let Some(max_index) = max_index {
            if num_variables <= *max_index {
                return Err(GLPKError::IndexOutOfBounds(num_variables, *max_index));
            }
        }
        return Ok(())
    }
}
