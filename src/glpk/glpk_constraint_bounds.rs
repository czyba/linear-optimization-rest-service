#[derive(Clone, Debug)]
pub enum GLPKConstraintBounds {
    LowerBound(f64),
    UpperBound(f64),
    BothBounds(f64, f64),
}

impl GLPKConstraintBounds {
    pub fn bound_type(&self) -> libc::c_int {
        return match self {
            GLPKConstraintBounds::LowerBound(_) => 2,
            GLPKConstraintBounds::UpperBound(_) => 3,
            GLPKConstraintBounds::BothBounds(_,_) => 4,
        }
    }

    pub fn get_lower_bound_or_null(&self) -> libc::c_double {
        return match self {
            GLPKConstraintBounds::UpperBound(_) => 0f64,
            GLPKConstraintBounds::LowerBound(v) => *v,
            GLPKConstraintBounds::BothBounds(v,_) => *v,
        }
    }

    pub fn get_upper_bound_or_null(&self) -> libc::c_double {
        return match self {
            GLPKConstraintBounds::LowerBound(_) => 0f64,
            GLPKConstraintBounds::UpperBound(v) => *v,
            GLPKConstraintBounds::BothBounds(_,v) => *v,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn lower_bound_return_2_as_type() {
        assert_eq!(2, GLPKConstraintBounds::LowerBound(0f64).bound_type());
    }

    #[test]
    fn lower_bound_returns_value_as_lower_bound() {
        let val = 7f64;
        assert_eq!(val, GLPKConstraintBounds::LowerBound(val).get_lower_bound_or_null())
    }

    #[test]
    fn lower_bound_returns_0_as_upper_bound() {
        let val = 7f64;
        assert_eq!(0f64, GLPKConstraintBounds::LowerBound(val).get_upper_bound_or_null())
    }

    #[test]
    fn upper_bound_return_3_as_type() {
        assert_eq!(3, GLPKConstraintBounds::UpperBound(0f64).bound_type());
    }

    #[test]
    fn upper_bound_returns_0_as_lower_bound() {
        let val = 7f64;
        assert_eq!(0f64, GLPKConstraintBounds::UpperBound(val).get_lower_bound_or_null())
    }

    #[test]
    fn upper_bound_returns_value_as_upper_bound() {
        let val = 7f64;
        assert_eq!(val, GLPKConstraintBounds::UpperBound(val).get_upper_bound_or_null())
    }

    #[test]
    fn both_bound_return_4_as_type() {
        assert_eq!(4, GLPKConstraintBounds::BothBounds(0f64, 0f64).bound_type());
    }

    #[test]
    fn both_bound_returns_0_as_lower_bound() {
        let val = 7f64;
        assert_eq!(-val, GLPKConstraintBounds::BothBounds(-val, val).get_lower_bound_or_null())
    }

    #[test]
    fn both_bound_returns_value_as_upper_bound() {
        let val = 7f64;
        assert_eq!(val, GLPKConstraintBounds::BothBounds(-val, val).get_upper_bound_or_null())
    }
}
