pub enum GLPKStatus {
    Optimal(f64),
    Feasible(f64),
    Infeasible,
    NoFeasible,
    Unbounded(f64),
    Undef
}

impl GLPKStatus {
    pub fn from_int_status_and_objective_value(status: i32, objective_value: f64) -> GLPKStatus {
        match status {
            2 => GLPKStatus::Feasible(objective_value),
            3 => GLPKStatus::Infeasible,
            4 => GLPKStatus::NoFeasible,
            5 => GLPKStatus::Optimal(objective_value),
            6 => GLPKStatus::Unbounded(objective_value),
            _ => GLPKStatus::Undef
        }
    }
}
