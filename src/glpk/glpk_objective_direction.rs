pub enum GLPKObjectiveDirection {
    Min,
    Max
}

impl GLPKObjectiveDirection {
    pub fn to_glpk_dir(&self) -> libc::c_int {
        return match self {
            GLPKObjectiveDirection::Min => 1,
            GLPKObjectiveDirection::Max => 2,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn min_returns_1_as_glpk_dir() {
        assert_eq!(1, GLPKObjectiveDirection::Min.to_glpk_dir())
    }

    #[test]
    fn max_returns_2_as_glpk_dir() {
        assert_eq!(2, GLPKObjectiveDirection::Max.to_glpk_dir())
    }
}