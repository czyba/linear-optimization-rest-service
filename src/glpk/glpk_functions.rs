/// Opaque forward declaration of the glp_prob struct defined in <glpk.h>
#[repr(C)] pub struct glp_prob { _private: [u8; 0] }

/// Opaque forward declaration of the glp_smcp struct defined in <glpk.h>
#[repr(C)] pub struct glp_smcp { _private: [u8; 0] }

/// Opaque forward declaration of the glp_iocp struct defined in <glpk.h>
#[repr(C)] pub struct glp_iocp { _private: [u8; 0] }

#[link(name = "glpk")]
extern {
    pub fn glp_create_prob() -> *mut glp_prob;
    pub fn glp_add_rows(ptr: *mut glp_prob, num: libc::c_int) -> libc::c_int;
    pub fn glp_delete_prob(ptr: *mut glp_prob);
    pub fn glp_get_num_rows(ptr: *mut glp_prob) -> libc::c_int;
    pub fn glp_add_cols(ptr: *mut glp_prob, num: libc::c_int) -> libc::c_int;
    pub fn glp_get_num_cols(ptr: *mut glp_prob) -> libc::c_int;
    pub fn glp_set_col_bnds(ptr: *mut glp_prob, i: libc::c_int, bound_type: libc::c_int, lb: libc::c_double, ub: libc::c_double);
    pub fn glp_simplex(glp_prob: *mut glp_prob, glp_smcp: *const glp_smcp) -> libc::c_int;
    pub fn glp_set_obj_coef(ptr: *mut glp_prob, j: libc::c_int, coefficient: libc::c_double);
    pub fn glp_set_obj_dir(ptr: *mut glp_prob, dir: libc::c_int);
    pub fn glp_set_col_kind(ptr: *mut glp_prob, i: libc::c_int, variable_type: libc::c_int);
    pub fn glp_intopt(glp_prob: *mut glp_prob, glp_iocp: *const glp_iocp) -> libc::c_int;
    pub fn glp_set_mat_row(glp_prob: *mut glp_prob, row: libc::c_int, len: libc::c_int, indices: *const libc::c_int, values: *const libc::c_double);
    pub fn glp_set_row_bnds(ptr: *mut glp_prob, i: libc::c_int, bound_type: libc::c_int, lb: libc::c_double, ub: libc::c_double);
    pub fn glp_get_status(ptr: *mut glp_prob) -> libc::c_int;
    pub fn glp_get_obj_val(ptr: *mut glp_prob) -> libc::c_double;
}