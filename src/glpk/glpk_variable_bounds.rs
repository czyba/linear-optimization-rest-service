
#[derive(Clone, Debug)]
pub enum GLPKVariableBounds {
    NoBounds,
    LowerBound(f64),
    UpperBound(f64),
    BothBounds(f64, f64),
}

impl GLPKVariableBounds {
    pub fn bound_type(&self) -> libc::c_int {
        return match self {
            GLPKVariableBounds::NoBounds => 1,
            GLPKVariableBounds::LowerBound(_) => 2,
            GLPKVariableBounds::UpperBound(_) => 3,
            GLPKVariableBounds::BothBounds(_,_) => 4,
        }
    }

    pub fn get_lower_bound_or_null(&self) -> libc::c_double {
        return match self {
            GLPKVariableBounds::NoBounds => 0f64,
            GLPKVariableBounds::UpperBound(_) => 0f64,
            GLPKVariableBounds::LowerBound(v) => *v,
            GLPKVariableBounds::BothBounds(v,_) => *v,
        }
    }

    pub fn get_upper_bound_or_null(&self) -> libc::c_double {
        return match self {
            GLPKVariableBounds::NoBounds => 0f64,
            GLPKVariableBounds::LowerBound(_) => 0f64,
            GLPKVariableBounds::UpperBound(v) => *v,
            GLPKVariableBounds::BothBounds(_,v) => *v,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;


    #[test]
    fn no_bound_return_1_as_type() {
        assert_eq!(1, GLPKVariableBounds::NoBounds.bound_type());
    }

    #[test]
    fn no_bound_returns_value_as_lower_bound() {
        assert_eq!(0f64, GLPKVariableBounds::NoBounds.get_lower_bound_or_null())
    }

    #[test]
    fn no_bound_returns_0_as_upper_bound() {
        assert_eq!(0f64, GLPKVariableBounds::NoBounds.get_upper_bound_or_null())
    }

    #[test]
    fn lower_bound_return_2_as_type() {
        assert_eq!(2, GLPKVariableBounds::LowerBound(0f64).bound_type());
    }

    #[test]
    fn lower_bound_returns_value_as_lower_bound() {
        let val = 7f64;
        assert_eq!(val, GLPKVariableBounds::LowerBound(val).get_lower_bound_or_null())
    }

    #[test]
    fn lower_bound_returns_0_as_upper_bound() {
        let val = 7f64;
        assert_eq!(0f64, GLPKVariableBounds::LowerBound(val).get_upper_bound_or_null())
    }

    #[test]
    fn upper_bound_return_3_as_type() {
        assert_eq!(3, GLPKVariableBounds::UpperBound(0f64).bound_type());
    }

    #[test]
    fn upper_bound_returns_0_as_lower_bound() {
        let val = 7f64;
        assert_eq!(0f64, GLPKVariableBounds::UpperBound(val).get_lower_bound_or_null())
    }

    #[test]
    fn upper_bound_returns_value_as_upper_bound() {
        let val = 7f64;
        assert_eq!(val, GLPKVariableBounds::UpperBound(val).get_upper_bound_or_null())
    }

    #[test]
    fn both_bound_return_4_as_type() {
        assert_eq!(4, GLPKVariableBounds::BothBounds(0f64, 0f64).bound_type());
    }

    #[test]
    fn both_bound_returns_0_as_lower_bound() {
        let val = 7f64;
        assert_eq!(-val, GLPKVariableBounds::BothBounds(-val, val).get_lower_bound_or_null())
    }

    #[test]
    fn both_bound_returns_value_as_upper_bound() {
        let val = 7f64;
        assert_eq!(val, GLPKVariableBounds::BothBounds(-val, val).get_upper_bound_or_null())
    }
}