use super::super::routes::Page;
use super::super::routes::linear_program::*;
use super::ServiceError;
use super::solve_linear_program::solve_linear_program;

use super::schema::linear_problem;

pub use diesel::prelude::*;

#[derive(Queryable, Debug)]
pub struct PostgresLinearProblem {
    pub id: i32,
    pub data: Vec<u8>,
}

#[derive(Insertable, Debug)]
#[table_name="linear_problem"]
pub struct NewPostgresLinearProblem {
    pub data: Vec<u8>,
}

pub fn get_linear_programs(page: u32, page_size: u32) -> Result<Page<LinearProgram>, ServiceError> {
    requires_connection!(connection, {
        use super::schema::linear_problem::dsl::*;
        let results = linear_problem
            .limit(page_size as i64)
            .load::<PostgresLinearProblem>(connection)?;

        let mut ret_val = Vec::with_capacity(results.len());
        for pglp in results {
            let data_str = std::str::from_utf8(&pglp.data)?;
            let result : InputLinearProgram = serde_json::from_str(&data_str)?;
            ret_val.push(LinearProgram::from_id_and_data(pglp.id, result));
        }

        return Ok(Page {
            total_pages: 1, //TODO
            elements: ret_val,
        })
    })
}

pub fn get_linear_program(id: i32) -> Result<Option<LinearProgram>, ServiceError> {
    requires_connection!(connection, {
        use super::schema::linear_problem;
        let read_data = linear_problem::table.find(id)
            .get_result::<PostgresLinearProblem>(connection)?;

        let reader = std::io::Cursor::new(&read_data.data);
        let result_without_id : InputLinearProgram = serde_json::from_reader(reader)?;
        let result = LinearProgram::from_id_and_data(read_data.id, result_without_id);

        Ok(Some(result))
    })
}

pub fn get_linear_program_solution(id: i32) -> Result<Option<Solution>, ServiceError> {
    requires_connection!(connection, {
        use super::schema::linear_problem;
        let read_data = linear_problem::table.find(id)
            .get_result::<PostgresLinearProblem>(connection)?;

        let reader = std::io::Cursor::new(&read_data.data);
        let result_without_id : InputLinearProgram = serde_json::from_reader(reader)?;
        let linear_problem = LinearProgram::from_id_and_data(read_data.id, result_without_id);

        solve_linear_program(&linear_problem).map(|solution| { Some(solution) })
    })
}

pub fn add_new_linear_program(data: InputLinearProgram) -> Result<LinearProgram, ServiceError> {
    requires_connection!(connection, {
        use super::schema::linear_problem;
        let data_to_save = NewPostgresLinearProblem {
            data: serde_json::to_vec(&data)?,
        };

        let read_data : PostgresLinearProblem = diesel::insert_into(linear_problem::table)
            .values(&data_to_save)
            .get_result(connection)?;

        let reader = std::io::Cursor::new(&read_data.data);
        let result_without_id : InputLinearProgram = serde_json::from_reader(reader)?;
        let result = LinearProgram::from_id_and_data(read_data.id, result_without_id);

        Ok(result)
    })
}

pub fn replace_linear_program(
    id: i32,
    data: InputLinearProgram,
) -> Result<LinearProgram, ServiceError> {
    requires_connection!(connection, {
        use super::schema::linear_problem;
            let data_to_save: Vec<u8> = serde_json::to_vec(&data)?;

        let read_data = diesel::update(linear_problem::table.find(id))
            .set(linear_problem::data.eq(data_to_save))
            .get_result::<PostgresLinearProblem>(connection)?;

        let reader = std::io::Cursor::new(&read_data.data);
        let result_without_id : InputLinearProgram = serde_json::from_reader(reader)?;
        let result = LinearProgram::from_id_and_data(read_data.id, result_without_id);

        Ok(result)
    })
}

pub fn remove_linear_program(id: i32) -> Result<Option<LinearProgram>, ServiceError> {
    requires_connection!(connection, {
        use super::schema::linear_problem;
        let read_data: PostgresLinearProblem =
        diesel::delete(linear_problem::table.filter(linear_problem::id.eq(id)))
            .returning(linear_problem::all_columns)
            .get_result(connection)?;
        let reader = std::io::Cursor::new(&read_data.data);
        let result_without_id : InputLinearProgram = serde_json::from_reader(reader)?;
        let result = LinearProgram::from_id_and_data(read_data.id, result_without_id);

        Ok(Some(result))
    })
}
