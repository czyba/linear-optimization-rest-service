#[macro_use]
pub mod db;
pub mod linear_program;
pub mod solve_linear_program;
pub mod schema;

pub enum ServiceError {
    CouldNotAcquireConnection,
    SerializationError,
    GLPKError,
}

impl From<std::str::Utf8Error> for ServiceError {
    fn from(_err: std::str::Utf8Error) -> ServiceError {
        ServiceError::SerializationError
    }
}

impl From<serde_json::error::Error> for ServiceError {
    fn from(_err: serde_json::error::Error) -> ServiceError {
        ServiceError::SerializationError
    }
}
