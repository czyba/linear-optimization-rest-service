pub use diesel::prelude::*;
use r2d2::Pool;
use diesel::pg::PgConnection;
use diesel::r2d2::ConnectionManager;

use super::ServiceError;
use std::cell::RefCell;

lazy_static! {
    static ref __CONNECTION_POOL: Pool<ConnectionManager<PgConnection>> = {
        let cm = ConnectionManager::<PgConnection>::new(r"postgres://postgres:postgres@172.17.0.2:5432/diesel");
        Pool::builder()
            .min_idle(Some(2u32))
            .max_size(10u32)
            .build_unchecked(cm)
    };
}

pub fn get_connection_pool() -> &'static Pool<ConnectionManager<PgConnection>> {
    return &__CONNECTION_POOL;
}

thread_local!(
    pub static __CONNECTION: RefCell<Option<r2d2::PooledConnection<ConnectionManager<PgConnection>>>> = RefCell::new(None);
);

pub struct DropConnection<'a> {
    pub __connection_optional: &'a std::cell::RefCell<Option<r2d2::PooledConnection<ConnectionManager<PgConnection>>>>,
}

impl<'a> Drop for DropConnection<'a> {
    fn drop(&mut self) {
        self.__connection_optional.replace(None);
    }
}

#[macro_use]
macro_rules! requires_connection {
    ($connection_name: ident, $code: expr) => ({
        use crate::services::db::__CONNECTION;
        __CONNECTION.with(|__connection_box_optional| {
            use crate::services::db::DropConnection;
            let mut __connection_returner = None;
            {
                // The additional boolean is used in order to avoid borrowing the value twice,
                // once mutable and once immutably, which results in a panic!
                let mut __requires_connection = false;
                if let Option::None = *__connection_box_optional.borrow() {
                    __requires_connection = true;
                }
                if __requires_connection {
                    use crate::services::db::get_connection_pool;
                    let __guard = get_connection_pool();
                    __connection_box_optional.replace(Some(__guard.get()?));
                    __connection_returner = Some(DropConnection{__connection_optional: &__connection_box_optional});
                }
            }

            let mut __connection_optional = __connection_box_optional.borrow();
            // Getting a connection should never fail after we acquired one.
            let $connection_name = __connection_optional.as_ref().unwrap();
            $code
        })
    })
}

impl From<r2d2::Error> for ServiceError {
    fn from(_err: r2d2::Error) -> ServiceError {
        ServiceError::CouldNotAcquireConnection
    }
}

impl From<diesel::result::Error> for ServiceError {
    fn from(_err: diesel::result::Error) -> ServiceError {
        ServiceError::CouldNotAcquireConnection
    }
}
