use super::ServiceError;
use super::super::routes::linear_program::*;
use super::super::glpk::*;

fn calculate_max_variable_index(lp: &LinearProgram) -> Option<usize> {
    let mut max_var_index : Option<usize> = None;
    // First check objective function
    let possible_max_index = lp.objective_function.coefficients.iter()
        .last();
    if let Some((possible_max_index, _value)) = possible_max_index {
        match max_var_index {
            Some(v) => max_var_index = if v < *possible_max_index { Some(*possible_max_index) } else { Some(v) },
            None => max_var_index = Some(*possible_max_index),
        }
    }
    // Then check all constraints
    let possible_max_index = lp.constraints
        .iter()
        // not being empty implies the iterator has a last. Thus unwrap is safe.
        .filter(|constraint| !constraint.values.is_empty())
        .map(|constraint| *constraint.values.iter().last().unwrap().0)
        .max();
    if let Some(possible_max_index) = possible_max_index {
        match max_var_index {
            Some(v) => max_var_index = if v < possible_max_index { Some(possible_max_index) } else { Some(v) },
            None => max_var_index = Some(possible_max_index),
        }
    }
    // Finally check variable bounds
    let possible_max_index = lp.variable_bounds.iter()
        .last();
    if let Some((possible_max_index, _value)) = possible_max_index {
        match max_var_index {
            Some(v) => max_var_index = if v < *possible_max_index { Some(*possible_max_index) } else { Some(v) },
            None => max_var_index = Some(*possible_max_index),
        }
    }
    max_var_index
}

fn calculate_problem_variables(lp: &LinearProgram, num_vars: usize) -> Vec<GLPKVariableDescription> {
    let mut vars = Vec::with_capacity(num_vars);
    for index in 0..num_vars {
        let mut description = GLPKVariableDescription {
            variable_bounds: GLPKVariableBounds::NoBounds,
            variable_type: GLPKVariableType::Continous,
            variable_coefficient: 0f64,
        };

        if let Some(val) = lp.objective_function.coefficients.get(&index) {
            description.variable_coefficient = *val;
        }

        if let Some(var_desc) = lp.variable_bounds.get(&index) {
            description.variable_bounds = map_variable_bounds(&var_desc.bounds);
            description.variable_type = map_variable_type(&var_desc.variable_type);
        }
        vars.push(description);
    }
    vars
}

fn calculate_constraints(lp: &LinearProgram) -> Vec<GLPKConstraint> {
    let mut constraints = Vec::with_capacity(lp.constraints.len());
    for constraint in lp.constraints.iter() {
        constraints.push(GLPKConstraint {
            constraint_bounds: map_constraint_bound(&constraint.comparator),
            coefficients: constraint.values.clone()
        });
    }
    constraints
}

pub fn solve_linear_program(lp: &LinearProgram) -> Result<Solution, ServiceError> {
    let mut problem = GLPKProblem::new(
        map_objective_goal(&lp.objective_function.goal)
    )?;

    let max_var_index = calculate_max_variable_index(lp);
    let max_var_index = max_var_index.ok_or(ServiceError::GLPKError)?;
    let num_vars = max_var_index + 1;
    let vars = calculate_problem_variables(lp, num_vars);
    problem.add_variables(vars)?;

    let constraints = calculate_constraints(&lp);
    problem.add_constraint(&constraints)?;
    problem.solve();

    Ok(Solution{
        status: map_glpk_status_to_solution_status(problem.get_status()),
    })
}

fn map_glpk_status_to_solution_status(status: GLPKStatus) -> ResultStatus {
    match status {
        GLPKStatus::Feasible(obj) => ResultStatus::Feasible(obj),
        GLPKStatus::Infeasible => ResultStatus::Infeasible,
        GLPKStatus::NoFeasible => ResultStatus::NoFeasible,
        GLPKStatus::Optimal(obj) => ResultStatus::Optimal(obj),
        GLPKStatus::Unbounded(obj) => ResultStatus::Unbounded(obj),
        GLPKStatus::Undef => ResultStatus::Undef,
    }
}

impl From<GLPKError> for ServiceError {
    fn from(_error: GLPKError) -> ServiceError {
        ServiceError::GLPKError
    }
}

fn map_constraint_bound(comparison: &RowComparison) -> GLPKConstraintBounds {
    match comparison {
        RowComparison::LE(ub) => GLPKConstraintBounds::UpperBound(*ub),
        RowComparison::GE(lb) => GLPKConstraintBounds::LowerBound(*lb),
    }

}

fn map_variable_type(variable_type: &VariableType) -> GLPKVariableType {
    match variable_type {
        VariableType::BOOL => GLPKVariableType::Boolean,
        VariableType::INTEGER => GLPKVariableType::Integer,
        VariableType::RATIONAL => GLPKVariableType::Continous,
    }
}

fn map_variable_bounds(bounds: &VariableBounds) -> GLPKVariableBounds {
    match bounds {
        VariableBounds::NoBounds => GLPKVariableBounds::NoBounds,
        VariableBounds::LowerBound(v) => GLPKVariableBounds::LowerBound(*v),
        VariableBounds::UpperBound(v) => GLPKVariableBounds::UpperBound(*v),
        VariableBounds::BothBounds(lb, ub) => GLPKVariableBounds::BothBounds(*lb, *ub),
    }
}

fn map_objective_goal(goal: &ObjectiveFunctionGoal) -> GLPKObjectiveDirection {
    match goal {
        ObjectiveFunctionGoal::MIN => GLPKObjectiveDirection::Min,
        ObjectiveFunctionGoal::MAX => GLPKObjectiveDirection::Max,
    }
}
